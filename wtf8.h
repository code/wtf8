/**
 * Copyright (C) 2016--2019, 2021 Tom Ryder <tom@sanctum.geek.nz>
 *
 * This file is part of wtf8.
 *
 * wtf8 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * wtf8 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * wtf8.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define PROGRAM_NAME "wtf8"

#define BYTE_SEP '-'
#define CHAR_SEP ' '

int is_utf8_cont(unsigned char);
void print_octets(FILE *, char *);
void print_characters(FILE *, char *);
