wtf8
====

Tiny utility for interactive use to inspect the octets of UTF-8 strings in line
with the characters. Expects a single argument, a string.

    $ LANG=C.UTF-8
    $ export LANG
    $ make
    $ sudo make install
    $ wtf8 'test'
     74 65 73 74
      t  e  s  t
    $ wtf8 'tøást'
     74 c3-b8 c3-a1 73 74
      t     ø     á  s  t
    $ wtf8 'か'
     e3-81-8b
            か
    $ wtf8 '💦'
     f0-9f-92-a6
               💦

Dependencies
------------

None as far as I can tell apart from libc, but I've only tested it on GNU/Linux
with glibc, and on OpenBSD. Should build with GCC or Clang.

Author
------

Tom Ryder <tom@sanctum.geek.nz>

License
-------

Copyright (C) 2016--2019, 2021 Tom Ryder <tom@sanctum.geek.nz>

Distributed under GNU General Public License version 3 or any later version.
Please see `COPYING`.
