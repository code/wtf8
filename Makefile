.POSIX:
.PHONY: all install clean
PREFIX = /usr/local
ALL = wtf8
all: $(ALL)
install: all
	cp -- $(ALL) $(PREFIX)/bin
wtf8: wtf8.c wtf8.h
clean:
	rm -f -- $(ALL)
